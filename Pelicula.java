package Pelicula2;

import java.time.Year;

public class Pelicula {
	
		private String titulo;
		private Year anno;
		private String sinopsis;
		private Genero genero;
		private String pais;
		
		
		public String getTitulo() {
			return titulo;
		}
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		public Year getAnno() {
			return anno;
		}
		public void setAnno(Year anno) {
			this.anno = anno;
		}
		public String getSinopsis() {
			return sinopsis;
		}
		public void setSinopsis(String sinopsis) {
			this.sinopsis = sinopsis;
		}
		public Genero getGenero() {
			return genero;
		}
		public void setGenero(Genero genero) {
			this.genero = genero;
		}
		public String getPais() {
			return pais;
		}
		public void setPais(String pais) {
			this.pais = pais;
		}
		public java.util.Collection actor = new java.util.TreeSet();
		public java.util.Collection director = new java.util.TreeSet();
		public java.util.Collection productor = new java.util.TreeSet();
		public java.util.Collection guionista = new java.util.TreeSet();
		
		

